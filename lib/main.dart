import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:go_router/go_router.dart';
import 'package:riverpod_study/history.dart';

void main() {
  runApp(ProviderScope(
      child: MaterialApp(
    title: 'Flutter Demo',
    
    home: MyApp(),
  )));
}

final myprovider = StateProvider<int>((ref) {
  return 0;
});

final indexprovider = StateProvider<int>((ref) {
  return 0;
});

final GoRouter routerui = GoRouter(routes: [
  GoRoute(
    path: '/',
    builder: (context, state) {
      return const MyCounter();
    },
  ),
  GoRoute(
    path: '/history',
    builder: (context, state) {
      return const History();
    },
  )
]);

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp.router(theme: ThemeData(
      colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple),
      useMaterial3: true,
    ),
      routerConfig: routerui,
    );
  }
}

class MyCounter extends ConsumerWidget {
  const MyCounter({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context, WidgetRef ref) {
    print('object');
    ref.listen(myprovider, (previous, next) {
      if (next <= -1 && previous! >= 0) {
        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
          showCloseIcon: true,
          behavior: SnackBarBehavior.floating,
          content: Text('Alert You are going negative '),
          elevation: 20,
          shape: RoundedRectangleBorder(),
          backgroundColor: Colors.red,
        ));
      }
    });
    int _currentIndex = 0;

    return Scaffold(floatingActionButtonLocation: FloatingActionButtonLocation.endDocked,
      bottomNavigationBar:
        Row(mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: IconButton.filled(onPressed: () {
                    return context.go('/history');
                  }, icon: Icon(Icons.history_edu),splashRadius: 2),
            ),
          ],
        ),
      
      appBar: AppBar(
        title: Text(
          'Riverpod Counter',
          style: TextStyle(letterSpacing: 5),
        ),
      ),
      body: Center(
        child: Container(
          constraints: const BoxConstraints(maxHeight: 200, minWidth: 200),
          decoration: const BoxDecoration(
              color: Color.fromARGB(136, 120, 156, 155),
              borderRadius: BorderRadius.all(Radius.circular(22))),
          child: Padding(
            padding: const EdgeInsets.all(11),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Consumer(
                  builder: (context, ref, child) {
                    final name = ref.watch(myprovider);
                    return Text(
                      name.toString(),
                      style: TextStyle(fontSize: 30),
                    );
                  },
                ),
                SizedBox(
                  height: 15,
                ),
                ElevatedButton(
                  onPressed: () {
                    ref.read(myprovider.notifier).update((state) {
                      return state + 1;
                    });
                  },
                  child: Text('Incriment'),
                ),
                SizedBox(
                  height: 9,
                ),
                ElevatedButton(
                  onPressed: () {
                    ref.read(myprovider.notifier).update((state) {
                      return state - 1;
                    });
                  },
                  child: Text('Decriment'),
                ),
              ],
            ),
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
          onPressed: () {
            ref.invalidate(myprovider);
          },
          child: Icon(Icons.refresh)),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:go_router/go_router.dart';
import 'package:riverpod_study/main.dart';

class History extends MyCounter {
  const History({super.key});

  @override
  Widget build(BuildContext context,WidgetRef ref) {
    return ElevatedButton(onPressed: () {
      return context.go('/');
    }, child: Icon(Icons.remove_circle));
  }
}